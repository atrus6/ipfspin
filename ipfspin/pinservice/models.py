from django.db import models

# Create your models here.
class Pin(models.Model):
    hash = models.CharField(max_length=46)
    expires = models.DateTimeField()

class PinCheck(models.Model):
    hash = models.CharField(max_length=46)
    ran = models.IntegerField()
