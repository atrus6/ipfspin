from django.contrib import admin
from .models import Pin, PinCheck

# Register your models here.
admin.site.register(Pin)
admin.site.register(PinCheck)
