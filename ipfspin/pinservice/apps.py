from django.apps import AppConfig


class PinserviceConfig(AppConfig):
    name = 'pinservice'
