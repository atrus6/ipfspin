from django.conf.urls import url, include

from . import views

urlpatterns = [
        url(r'^$', views.index, name='index'),
        url(r'^confirm_pay', views.confirm_pay, name='confirm_pay'),
        url(r'^pin_content', views.pin_content, name='pin_content'),
        ]
