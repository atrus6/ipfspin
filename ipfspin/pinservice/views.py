from django.shortcuts import render

from ipfspin import settings

from pinservice.models import Pin, PinCheck

import math
import datetime
import psutil
import random

import ipfsapi
api = ipfsapi.connect('127.0.0.1', 5001)

import bitmath

from coinbase.wallet.client import Client
client = Client(settings.api_key, settings.api_secret)

# Create your views here.

def get_size(api, hash):
    try:
        size = api.object_stat(hash)['CumulativeSize']
    except:
        size = -1
    human = bitmath.Byte(bytes=size).best_prefix().format("{value:.2f} {unit}")
    size = size/100000.0
    return math.ceil(size), human

def index(request):
    free = psutil.disk_usage('.').free
    data = {}
    data['space'] = bitmath.Byte(bytes=free).best_prefix().format("{value:.2f} {unit}")
    return render(request, template_name='index.html', context=data)

def confirm_pay(request):
    hash = request.GET.get('hash')
    months = int(request.GET.get('months'))

    size, human = get_size(api, hash)
    data = {}
    print(size)
    if size < 1:
        data['error'] = "Invalid hash. Please add a correct hash."
        return render(request, template_name='confirm_pay.html', context=data)
    else:
        data['hash'] = hash
        data['size'] = size
        data['human'] = human
        data['months'] = months
        data['cost'] = "{0:.8f}".format(size*months/100000000)


        satoshi = int(size)*months / 100000000
        ran = random.randint(0,10000000)
        pc = PinCheck(hash=hash, ran=ran)
        pc.save()

        checkout = client.create_checkout(amount=satoshi, currency='BTC', name='IPNS Pinning', success_url='http://127.0.0.1:8000/pin_content', metadata={'hash':hash, 'months':months, 'key':ran}, auto_redirect=True)

        data['code'] = checkout['embed_code']

        return render(request, template_name='confirm_pay.html', context=data)

def pin_content(request):
    hash = request.GET.get('order[metadata][hash]')
    time = int(request.GET.get('order[metadata][months]'))
    ran = request.GET.get('order[metadata][key]')

    pincheck = PinCheck.objects.filter(ran=ran).filter(hash=hash)

    if not pincheck:
        return render(request, template_name='index.html')
    else:
        pincheck.delete()

    already_pinned = Pin.objects.filter(hash=hash)

    if not already_pinned:
        api.pin_add(hash)
        pin = Pin()
        pin.hash = hash
        try:
            pin.expires = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=time*31)
        except OverflowError:
            pin.expires = datetime.datetime.max
        pin.save()
    else:
        pin = already_pinned.first()

        if pin.expires < datetime.datetime.now(datetime.timezone.utc):
            api.pin_add(hash)
            try:
                pin.expires = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=time*31)
            except OverflowError:
                pin.expires = datetime.datetime.max
        else:
            try:
                pin.expires = pin.expires + datetime.timedelta(days=time*31)
            except OverflowError:
                pin.expires = datetime.datetime.max

        pin.save()
    data = {'hash':hash, 'time':time}
    return render(request, template_name='complete.html', context=data)
